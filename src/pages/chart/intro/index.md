---
id: 'intro'
title: 介绍
---

**nova-chart** 包是我们在 D3 上自我实现的图表组件。目前实现的组件包括：line-chart、area-chart、bar-chart、pie-chart。更多的图表类型根据我们的产品需求可以追加实现。

### 依赖

**nova-chart** 是通过 [react-native-svg](https://github.com/react-native-community/react-native-svg) 来渲染图形的。因此要使用这个包，需要在你的 RN 项目中先添加 react-native-svg 这个项目。

同时，这个包还依赖了 d3 的一些项目。这些 d3 的项目都是纯 javscript 实现的，它们都同时支持 iOS 和 android。

> 好生意移动端的 RN 项目已经添加了 react-native-svg 项目，因此大家就不需要做这一步了。

### 动机

在 RN 生态中，优秀的可用作产品中的开源图表方案其实是很少很少的。常见的方案有：

- [react-native-charts-wrapper](https://github.com/wuxudong/react-native-charts-wrapper) 
- [react-native-chart](https://github.com/tomauty/react-native-chart) 
- [victory-native](https://github.com/FormidableLabs/victory-native)
- [react-native-pathjs-charts](https://github.com/capitalone/react-native-pathjs-charts)
- [react-native-svg-charts](https://www.npmjs.com/package/react-native-svg-charts)

既然已经有这么多的开源方案，为什么我们还要发明一个轮子？其实最主要的或者是决定性的考虑来自两个指标，那就是**性能**和**交互满足度**。而在这两个指标中，从我们产品的实际需求出发，**交互满足度**的重要性要超过**性能**。而上面的开源项目中，我们无法找到任何一个项目可以同时满足这两个方面。

因此，最后我们只能选择自己发明轮子。在这里要感谢 [react-native-svg-charts](https://www.npmjs.com/package/react-native-svg-charts)项目的作者，在评估上面的项目时，我发现这个项目的代码最为简洁。一个原因是它基于 d3 这个优秀的开源可视化数据库，另外一个原因是它没有加入更多复杂的功能。它的代码最为容易理解和学习。

因此，我们的 nova-chart 库就借鉴或者说是脱胎于 [react-native-svg-charts](https://www.npmjs.com/package/react-native-svg-charts) 这个项目库。

接下来，让我们进一步了解 **nova-chart** 的使用。
