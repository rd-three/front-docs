---
id: ''
title: Native 插件
---


### 从手机相册选择图片

下面的代码示例了我们调用插件选择手机相册的图片。

```javascript{4}
import { nativePlugins } from 'nova-native-plugins';

async selectImages() {
    const result = await nativePlugins.device.selectImages({ max: 3 });
    if (Array.isArray(result)) {
        result.forEach(image => {
            console.log(image);
        })
    }
} 
```

`nativePlugins.device.selectImages` 接受一个 `Object` 参数，我们可以通过设置 `max` 值限制用户在相册中可选择的图片数量。默认为 1。这个方法返回一个 `Promise<string[]>`。返回的数组中的每一个项是图片在手机上的文件路径。

当我们拿到插件返回的数据后，可以使用 `Image` 组件展示出来，如下面的代码：

```javascript
const image_url = result[0];
...
<Image
	resizeMode="contain"
	style={{ width: 80, height: 80 }}
	source={{
		uri: image_url
	}}
/>
```



### 调用手机摄像头拍照

就像上面选择相册图片一样，调用手机摄像头拍照也是一件很简单的事情。

```javascript{5}
import { nativePlugins } from 'nova-native-plugins';

async takePhoto() {
    try {
    	   const result = await nativePlugins.device.takePhoto();
    	   console.log('图片地址', result);
    } catch (error) {
    	   this.props.navigator.showAlert({
    		  message: error.message,
    		  alertType: 'error'
    	   });
    }
}
```


### 图片浏览

在弹出浮层浏览多张图片是很常见的需求，不用担心，我们只需要一句话就可以实现。

```javascript
const images = [...];

nativePlugins.ui.previewImage({
	urls: images,
	current: 1
});
```

`previewImage` 函数接受一个 `Object` 参数，这个参数可以提供下面的属性：

- `urls` - `string[]`
- `current` - `number`

### 选择日期和时间

需要设置单据日期类型字段的值？手工输入太麻烦了。我们提供了选择日期时间插件，通过滑动滚轮来轻松设置。

根据不同的使用场景，你可以设置【日期】、【时间】、【日期+时间】。

只需要一两行代码就可以。

1. 设置【日期】

   ```typescript
   const defaultDate = 1516692164;
   const result = await nativePlugins.ui.pickDate(defaultDate);
   ```

2. 设置【时间】

   ```typescript
   const defaultTime = 1516692164;
   const result = await nativePlugins.ui.pickTime(defaultTime);
   ```

3. 设置【日期+时间】

   ```typescript
   const defaultDateTime = 1516692164;
   const result = await nativePlugins.ui.pickDateTime(defaultDateTime);
   ```

三个接口都接受一个 number 类型的参数，这个参数是一个时间戳，用来设定日期时间控件加载后显示的日期时间。

如果这个参数没有设置，则默认显示当前时间。

### 条码识别 + 二维码识别

用手机的摄像头来识别商品的条形码和二维码也很简单，一行代码就搞定。

```typescript
const result = await nativePlugins.device.scanCode();
```

### 生成二维码图片

手机橱窗需要生成店铺的二维码图片进行分享，我们也提供了相应的插件，还是几行代码就搞定。

```typescript 
const args : GenerateQRCodeImageArgs = {
    /**
     * 二维码内容字符串
     */
    contentString: "http://www.chanjet.com", 
    /**
     * 生成二维码图片的大小
     */
    imageSize?: 300,
    /**
     * 二维码中间 Logo 图片的地址
     */
    logoImageUrl?: "http://xxxx.com/myStoreIcon.png",
    /**
     * Logo 图片的大小
     */
    logoImageSize?: 50,
    /**
     * 二维码纠错能力，默认为 H
     */
    correctionLevel?: "H",//"L" | "M" | "Q" | "H";
}
const result = await nativePlugins.device.generateQRCodeImage(args);
```

接口接受一个 GenerateQRCodeImageArgs 类型的参数，返回结果是生成的二维码图片在手机上的文件路径。

### 剪贴板读写

1. copy 方法

   ```
   const result = await nativePlugins.device.copy("要 Copy 的内容");
   ```

2. paste 方法

   ```
   const result = await nativePlugins.device.paste();
   ```

   ​





