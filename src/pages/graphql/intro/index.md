---
id: 'intro'
title: 入门
---

在我们好生意的产品开发中，不管是 Web 端还是移动端，都需要大量的使用到 GraphQL 来进行数据查询和更新。因此，我们有必要将我们产品开发中和 GraphQL 有关的一些内容进行详细的讲解。

注意：这里不会对 GraphQL 本身的规范进行详细说明。要求我们的阅读者先花些时间去官网了解一下 GraphQL 的规范。[传送门在这里](http://graphql.org/learn/)。

## 好生意基于 BO 模型的 GraphQL

### BO 查询

我们使用 **GraphQL** 最常见的场景是查询好生意的业务数据（我们称之为 **BO**），针对 BO 的查询的语法都是统一的。

假设我们有以下的 BO 模型：

**Customer**

```json
Long id
String name
ForeignKey owner reference to User

```

**Contact**

```json
Long id
String name
ForeignKey customer reference to Customer
```

**Order**

```json
Long id
String subject
ForeignKey shippingCustomer reference to Customer
ForeignKey paymentCustomer reference to Customer
```

**OrderItem**

```json
Long id
ForeignKey product reference to product
ForeignKey parent reference to Order
Decimal quantity
Decimal amount
```

**Prodcut**

````json
Long id
String name
````

**User**

```json
Long id
String name
```

 这样 Customer, Contact, Order, OrderItem, Product, User 对象之间的关系如下:

- OrderItem (by product) ---> Product
- OrderItem (by parent) ---> Order
- Order (by shippingCustomer) ---> Customer
- Order (by paymentCustomer) ---> Customer
- Contact (by customer) ---> Customer
- Customer (by owner) ---> User

> 以上关系中，从左到右为1:1有关系，从右往左为1:n关系

这些 BO 会按照一定的规则自动生成下面的 Schema：

```json
Customer {
	 Long id,
	 String name,
	 User owner {Long id, String name},
	 List<Order> shippingOrders,
	 List<Order> paymentOrders,
	 List<Contact> contacts,
     Integer shippingOrders__count,
     Integer paymentOrders__count,
     Integer contacts__count)
}

Contact{
	Long id,
	String name,
	Customer customer{Long id, String name, ...}
}

Order{
	Long id,
	String subject,
	Customer shippingCustomer{Long id, String name, ...},
	Customer paymentCustomer{Long id, String name, ...},
	Contact contact{Long id, Sting name, ...},
	List<OrderItem> OrderItems
}

OrderItem {
	Long id,
	Product product{Long id, String name},
	Order parent{Long id, String subject, ...},
	Long quantity,
	Float amount
}

Product{
	Long id,
	String name
}

User{
	Long id,
	String name
}
```

#### 查询参数

我们可以看到，每一个**BO** 都有一个同名的 **QueryType** （查询对象类型)，比如 Customer 这个 BO 就会有一个同名的 QueryType。每一个 BO QueryType 支持的查询参数都是一样的。

- **criteriaStr**

  查询条件，这个查询条件类似于 SQL 中的 WHERE 部分。从基本的语法上来说，criteriaStr 支持的语法是 **mysql** 的 **where** 语法。

  这个表达式中查询的字段是 GraphQL Schema 中的字段名，这些字段名通常与BO字段名一致。

  对 FK 属性（外键引用类型），GraphQL增加了关联到BO的字段。如对于 Contact 的查询条件 customer = 1外，由于customer是一个外键属性，因此还支持Contact.customer.xxx字段，表示该字段可以关联到Customer, 即针对这种BO字段可以级联查询。如对 Contact 列表进行查询时，以下以条件是合法的： customer.name like '%admin' or customer.owner.name='Amdin'。

  另外 criteriaSt r中可以引入子查询，子查询中，引用查询对象的字段时，要加上表别名。特别提醒：主对象的表别名为 "m"， 并且子查询目前只支持 from 中为单表子查询，如查询 Customer 对象时，条件可以是 exists (select * from Contact where name='张三' and id=m.id) 或者 id in (select c.customer from Contact as c where c.name='张三')。

  > criteriaStr中的子查询限制
  >
  > 1. 子查询中如果要使用主表的字段，必须加上别名，主表的别名为 m
  > 2. 子查询的 from 语句只支持单表查询，不支持多表 join 及子查询
  > 3. 子查询中不支持 order by, group by
  > 4. 子查询中的 where 语句可以支持子查询

- **firstResult**

  指定在符合条件的结果从第几个记录开始返回。

- **maxResult**

  指定在符合条件的结果，从 **firstResult** 开始最多返回几个记录。

- **sortBy**

  sortBy为一个数组，数组中每个排序字像为 { fieldName: "xxx", order: "asc|desc"}。 其中的 fieldName 与查询字段一样，可以指定级联字段排序，即对联系人查询时，customer.owner.name是合法的。

- **bindVars**

  出现在 criteriaStr 条件（包括子表条件）中的参数数组，数组中每个为一个参数对象 { name:"参数名", value: 参数值 }， 其中参数值的类型可以 **string,  number, boolean** 或者它们的数组或者构成的对象。

#### 示范

下面给出常用的一些查询示例: （示例只给出来的是 GraphQuery 查询参数中的 query 查询语法字符串）。

##### 1 查询所有Customer的列表，只包含id和name字段

查询语法：

​    ` { data: Customer{id,name}}`

结果：

```javascript
{
  "data": [
	{"id": 1, "name: "name1"},
	{"id": 1, "name: "name1"}
 ]
}
```

##### 2 查询所有Customer的列表，加上所有的User列表，都只包含id和name字段

查询语法：

​    ` {custList: Customer{id,name}, userList: User{id,name}}`

结果：

```javascript
{
  	"custList": [
	    {"id": 1, "name: "name1"},
	    ...,
	    {"id": 2, "name: "name2"}],
	"userList": [
	    {"id": 1, "name: "user1"},
	    ...,
	    {"id": 2, "name: "user2"}]
}
```

##### 3 查询Customer的列表，要求名称包含"北京"，只包含id和name字段

查询语法：

​    ` {data: Customer(criteriaStr:"name like '%北京%'"){id,name}}`

​     或者：

​    ` {data: Customer(criteriaStr:"name like :name", bindVars: [{name:"name",value:"%北京%"}]){id,name}}`

```js
{
  "data": [
	    {"id": 3, "name: "北京。。"},
	    ...,
	    {"id": 4, "name: "。。北京。。"}
  ]
}
```

##### 4 查询 Customer的列表，要求名称包含"北京"，只包含id和name字段，并要联查每个客户的付款的订单列表(要求订单编辑包含00)及明细

查询语法：

```js
{
  data: Customer(criteriaStr: "name like '%北京%'") {
    id
    name
    paymentCustomer(criteriaStr: "code like '%00%'") {
      id
      code
      items: parent {
        id
        quantity
        amout
      }
    }
  }
}
```

结果：

```js
{
  "data": [
	    {
		    "id": 3,
		    "name": "北京。。",
            "paymentCustomer": [
                {
                  "id": "1",
                  "code": "001",
                  "items": [
                      {"id": "1", "quantity": 1.0, "amount": 200.0},
                      {"id": "2", "quantity": 2.0, "amount": 300.0}
                  ]
                },
                ...
			 ]
		},
	    ...
    ]
}
```

##### 5 查询Customer的列表按ID排序，需要前10个客户，以及第一100到150个客户，只包含id和name字段

查询语法：

```js
{
  Customer(sortBy: [{fieldName: "id", order: "asc"}], firstResult: 0, maxResult: 10) {
    id
    name
  }
}

```

结果：

```js
{
  "data": [
	    {
		    "id": 3,
		    "name": "北京。。"
		},
	    ...
    ]
}
```

##### 6 查询 id为1的 Contact，包括 Contact 的关联 Customer, 以及 Customer 的关联 User

查询语法：

```js
{
  data: Contact(criteriaStr: "id=1") {
    id
    name
    customer {
      id
      name
      owner {
        id
        name
      }
    }
  }
}
```

结果：

```js
{
    "data":[
        {
            "id":1,
            "name":"contact1",
            "customer":{
                "id":1,
                "name":"customer1",
                "owner":{
                    "id":1,
                    "name":"user1"
                }
            }
        }
    ]
}
```

### BO 统计查询

有很多时候，我们不仅仅需要查询明细数据，还需要对数据做一些统计。这些统计有不少时候只是简单的统计要求，如果我们每一个统计都需要后端提供 REST 接口的话，显然是不敏捷的。而且接口数量也很容易产生 **爆炸**。

鉴于这种要求，我们的 BO GraphQL 提供了基本的统计查询支持。

- 为每一个 BO 建立了一个聚合查询类型： **Aggregate**\<BoName>， 返回列表数据，增加一个 **aggrOne**\<BoName> 返回一个对象（如果有多行，返回第一行）。

- 为 BO 中的 list 属性增加一个聚合子表查询字段: **aggregate**\<List field name>， 返回列表数据，增加一个 **aggrOne**\<List field name> 返回一个对象（如果有多行，返回第一行）。

- 聚合查询类型对象中, 字段分为：

    <ul>

    <li>数值类： 可以做为查询取统计字段，支持查询参数：
    <ul>

    <li><strong>aggr</strong>， 取值为SUM, MAX, MIN, AVG, DIM. 没有参数默认为SUM。
    </li>
    <li><strong>expr</strong>, 值为一个字符串，表示运算的表达式{0}表示当前字段。</li>

    </ul>
    </li>

    <li>非数值类的部分字段： 如果指定，表示以这些字段进行 group by， 支持参数：

    <ul><li><b>expr</b>, 值为一个字符串，表示运算的表达式 {0} 表示当前字段。</lil>

    </ul>

    </li>

    <li>aggrCount 是一个特殊字段，统计行数。</li>

    <li>aggrField 是一种外键bo级联字段，参数为：

    <ul>

    <li><b>fieldName</b>: 字符串，一个级联字段名，比如订单的customer字段，可以做为fieldName为 customer.owner.xxx</li>

    <li><b>expr</b>: 值为一个字符串，表示运算的表达式{0}表示当前字段</li>

    <li><b>aggr</b>: 取值为 SUM, MAX, MIN, AVG, DIM , 其中不能统计的字段，只能选择 DIM </li>

    </ul>

    </li>

    </ul>

- 聚合查询除了普通列表查询支持的参数外，还支持havingStr参数，表示对查询的结果再按条件过滤，其中 havingStr条件串中的字段必须是 gql 查询出来的字段名（如果有 gql 字段别名，必须为字段别名），如:

   ```js
   {
     AggregateSalesOrder(havingStr: "sumValue > 0 and aggrCount =25 ") {
       groupByUser: createdUserId {
         id
         name
       }
       aggrCount
       sumValue: totalDiscount
       sumValue2: totalDiscount(aggr: SUM)
       avgValue: totalDiscount(aggr: AVG)
     }
   }
   ```

   ​

- 聚合查询支持查询结果的排序， 如果排序的字段是查询结果中的字段，则表示按查询结果排序，其中字段有 gql 别名，必须使用别名， 如：

   ```js
   {
     AggregateSalesOrder(havingStr: "sumValue > 0 and aggrCount =25 ", sortBy: [{fieldName: "sumValue", order: "asc"}]) {
       groupByUser: createdUserId {
         id
         name
       }
       aggrCount
       sumValue: totalDiscount
       sumValue2: totalDiscount(aggr: SUM)
       avgValue: totalDiscount(aggr: AVG)
     }
   }
   ```

### 注意事项

- 对于时间类型的查询，由于数据库的精度与java不一样，条件查询时，需要改写，如： 

   ```
   {
     CustVendor(
       criteriaStr: "createdStamp >= FROM_UNIXTIME(:V0/1000) AND createdStamp <= FROM_UNIXTIME(:V1/1000)",
       bindVars: [{name: "V0", value: 1494240789000}, {name: "V1", value: 1494240789000}]) 
     {
       id
     }
   }
   ```

   如代码片段中所示：日期类型的比较需要使用 **FROM_UNIXTIME** 函数转换，注意 **V0/1000** 表示时间单位需要转换为秒。

   ​

- 查询条件中支持list类型的属性作为查询条件。如果是list属性，则自动按exists子查询实现（可能有list下还有list), 前提条件是：

   - 一个完整的逻辑表达式中，只能且唯一只有一个这样的list字段,不能有多个。
   - list属性只能作简单的比较，like, in, between， isnull运算, 且只能出现在左边，可以使用一个函数，不能再进行运算
   - 暂不支持通过rel表关联的list属性查询

- 聚合查询, 特殊字段 aggrField 增强：

   - fieldName参数支持级联字段,如： customer.owner.name
   - 增加参数referFields, 如果字段是一个外键字段，可以用它指定要查询的bo字段列表
   - 增加参数referKeyField, 如果字段是一个外键字段，但返回值不是id时，使用它指明返回值的字段,如treePath

   ​

