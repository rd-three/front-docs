const parseFilepath = require('parse-filepath');
const fs = require('fs');
const path = require('path');

module.exports = {
	// async onPostBuild({ graphql }) {
	// 	const result = await graphql(`
	//     {
	//       allSitePage {
	//         edges {
	//           node {
	//             path
	//           }
	//         }
	//       }
	//     }
	//   `);
	// 	generateSitemap(result.data.allSitePage.edges.map(({ node }) => node));
	// },
	onCreateNode({ node, boundActionCreators, getNode }) {
		const { createNodeField } = boundActionCreators;
		let slug;
		if (node.internal.type === 'MarkdownRemark') {
			const fileNode = getNode(node.parent);
			const parsedFilePath = path.parse(fileNode.relativePath);

			if (parsedFilePath.name !== 'index' && parsedFilePath.dir !== '') {
				slug = `/${parsedFilePath.dir}/${parsedFilePath.name}/`;
			} else if (parsedFilePath.dir === '') {
				slug = `/${parsedFilePath.name}/`;
			} else {
				slug = `/${parsedFilePath.dir}/`;
			}

			// Add slug as a field on the node.
			createNodeField({ node, name: 'slug', value: slug });
		}
	},
	async createPages({ graphql, boundActionCreators }) {
		const { createPage } = boundActionCreators;

		const template = path.resolve('src/templates/_docstemplate.jsx');
		// Query for all markdown "nodes" and for the slug we previously created.
		const result = await graphql(
			`
          {
            allMarkdownRemark {
              edges {
                node {
                  fields {
                    slug
                  }
                }
              }
            }
          }
        `
		);
		if (result.errors) {
			throw result.errors;
		}

		// Create pages.
		result.data.allMarkdownRemark.edges.forEach((edge) => {
			createPage({
				path: edge.node.fields.slug, // required
				component: template,
				context: {
					slug: edge.node.fields.slug
				}
			});
		});
	}
};
