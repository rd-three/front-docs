---
id: 'line-chart'
title: 折线图、面积图
---

折线图和面积图是一个应用中最常见的图表展示形式之一。我们提供的 line-chart 组件包含了折线图和面积图两种图形的实现。

![](media/15159868085707.png)

### 示例

```typescript
import { LineChart } from "nova-chart";

class LineChartExample extends React.Component<any, any> {
  render() {
    const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ];

    return (
      <LineChart
        style={{
       		height: 80
        }}
        data={data}
      />
    );
  }
}
```

### 属性


| 属性              | 默认值                                      | 描述                                       |
| --------------- | ---------------------------------------- | ---------------------------------------- |
| data            | **required**                             | 一个 number 类型数组。                          |
| labels          | undefined                                | labels 可以让开发者提供给图表数据点的格式化标签。labels 函数的声明是：(index: number, data) => string。index 是数据点在 data 中的位置。 |
| style           | undefined                                | [ViewStyleProps](https://facebook.github.io/react-native/docs/viewstyleproptypes.html) |
| svg             | {}                                       | 一个包含所有应该传递给 react-native-svg 组件对象的属性。[可用属性参考这里](https://github.com/react-native-community/react-native-svg#common-props) |
| curve           | "cardinal"                               | 数据点直接的插值算法。支持的插值算法有<br /> linear、step、stepBefore、stepAfter、basis、cardinal、monotoneX、catmullRom。 |
| contentInset    | { left: 8, top: 8, bottom: 8, right: 8 } | 设置画布的四边的边距。比如我们给 chart 的 width 为 200，<br />则实际绘制的区域的有效宽度是 200 减去 contentInset 的 left 和 right |
| renderGradient  | ({id, width, height, x, y}) => { LinearGradient } | 可以设置折线图的渐变渲染方式。                          |
| showArea        | true                                     | 折线图显示为面积图的样式。                            |
| showMarker      | true                                     | 当 showMarker 启用时，用户在折线图上通过按下手指或者滑动手指可以实时显示一个 **marker**，类似在 web 上鼠标滑过时显示 **tooltip** 一样。这样的交互方式可以解决在手机上通过手指来浏览数据点的详细数据。 |
| markerComponent |                                          | 通过这个函数，开发者可以自定义 marker 的显示样式。函数的声明为：({x, y, label, index, data}) => Component。返回的 component 是 react-native 组件。 |

### How

#### 自定义 Marker

实际开发产品中，UE 经常会希望 marker （tooltip）可以灵活的进行定制化显示。就如同下图一样，我们可以通过 **markerComponent** 函数提供 marker 的自定义显示。

![](media/15161035475668.png)

```typescript
<LineChart
   style={{
      height: 80
   }}
   data={data}
   markerComponent={this.renderMarker}
/>
...

 renderMarker(args: {
        x: number;
        y: number;
        label: string;
        index: number;
        data: any[];
    }) {
        const { index, data } = args;

        return (
            <View
                h-50
                w-100
                style={{
                    position: "absolute",
                    left: args.x - 50,
                    top: args.y - 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderColor: "green",
                    borderWidth: StyleSheet.hairlineWidth,
                    borderRadius: 5
                }}
            >
                <Text font-12 marginB-8 brandPrimary>{`本月 ${
                    args.index
                } 日`}</Text>
                <Text font-10 orange10>{`${data[index]} 箱 🐟🐟🐟`}</Text>
            </View>
        );
    }
```

