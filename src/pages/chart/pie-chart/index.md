---
id: 'pie-chart'
title: 饼图
---


![](media/15160197961951.png)

### 示例

```typescript
import { PieChart } from "nova-chart";

class BarChartExample extends React.Component<any, any> {
  render() {
    const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ];
	const pieData = data.filter(value => value > 0).map((value, index) => ({
            value,
            style: {
                fill: randomColor()
            }
        }));
    
    return (
      <PieChart
        style={{
       		height: 80
        }}
        data={pieData}
      />
    );
  }
}
```

### 属性

| 属性              | 默认值                        | 描述                                       |
| --------------- | -------------------------- | ---------------------------------------- |
| data            | **required**               | 一个对象类型数组。数据结构为：{style? StyleProps, key: string, value: number} |
| labels          | undefined                  | labels 可以让开发者提供给图表数据点的格式化标签。labels 函数的声明是：(index: number, data) => string。index 是数据点在 data 中的位置。 |
| style           | undefined                  | [ViewStyleProps](https://facebook.github.io/react-native/docs/viewstyleproptypes.html) |
| innerRadius     | "50%"                      | 内圆的半径百分比或者一个绝对值。                         |
| outerRadius     | undefined                  | 外圆的半径百分比或者一个绝对值。这个值可以绝对圆和画布边的距离。         |
| padAngle        | 0.005                      | 圆的每一块数据之间间距角度。                           |
| sort            | (a, b) => b.value -a.value | 对传入 data 数据如何排序。                         |
| showLabel       | true                       | 在每一个数据上显示数据的文本标签。可以通过 **labels** 格式化标签。  |
| renderInner     | undefined                  | 有时候，我们需要在圆的中心显示一些内容。renderInner 可以让我们自定义圆中心的显示内容。这个函数的声明是： (width?: number, height?: number) => any。函数返回值为一个 react component。 |
| onPointPressIn  | Function                   | 在用户按下一个数据块时触发这个事件。                       |
| onPointPressOut | Function                   | 用户取消一个数据块的按下状态时触发这个事件。                   |
| onPointPress    | undefined                  | 用户快速点击一个数据块时触发这个事件。                      |

#### onPointPressIn、onPointPressIn、onPointPress

这三个函数有相同的函数签名：

`(evt: ChartEventArgs) => ChartEventResult | undefined | null`

下面我们列出了 **ChartEventArgs**、**ChartEventResult** 的声明。

``` typescript
export interface ChartEventArgs {
    props: {
        style: StyleProps;
        index: number;
        data: any;
        label?: string;
    };
}

export interface ChartEventResult {
    target?: "data";
    mutation?: ChartMutation;
}

export interface ChartMutation {
    style?: StyleProps;
}

export interface StyleProps {
    fill?: string;
    stroke?: string;
    fillOpacity?: number;
    strokeOpacity?: number;
    [key: string]: any;
}
```

通常，在 **onPointPressIn** 和 **onPointPressOut** 的时候，我们需要对用户的操作进行一些视觉上的反馈处理。而这里的 **ChartMutation** 就可以满足我们的要求。

我们在 **pie-chart** 的源码中可以看到默认的 **onPointPressIn** 和 **onDataPointPressOut** 的定义：

```typescript
...        
onDataPointPressIn: ({ props }) => {
  return {
    mutation: {
      style: Object.assign(props.style, {
        fillOpacity: 0.75 
      })
    }
  };
},
onDataPointPressOut: ({ props }) => {
    return {
      mutation: {
        style: Object.assign(props.style, {
          fillOpacity: 1
        })
      }
    };
}
..
```

在 **onDataPointPressIn** 中，我们返回结果中包含了一个 **mutation**，这个 **mutation** 将当前数据的 **fillOpacity** 样式改为了 0.75，而在 **onDataPointPressOut**，我们用同样的方式又将 **fillOpacity** 样式还原为了 1。这样，我们就可以让用户看到按下手指和抬起手指时数据的显示效果发生了一些细微的变化，实现了交互反馈。

### How

#### 如何在饼图的中心区域加上内容？

有的时候，我们想要如下图一样在圆的中心区域加上一些内容。这可以通过 **renderInner** 函数来实现。
![](media/15161028991913.jpg)

```typescript
<PieChart
	style={{
       height: 80
    }}
    data={pieData}
	renderInner={this.renderInner}
/>
...

renderInner() {
  return (
    <View center>
    	<Text font-18>销售金额</Text>
    	<Text marginT-12>38万</Text>
    </View>
  );
}
```


