---
id: 'bar-chart'
title: 柱状图
---

![](media/15160854616677.png)

### 示例

```typescript
import { BarChart } from "nova-chart";

class BarChartExample extends React.Component<any, any> {
  render() {
      const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ];
	  const barData = [
			{
				values: data,
				positive: {
					fill: randomColor()
					// other react-native-svg supported props
				},
				negative: {
					fill: randomColor()
					// other react-native-svg supported props
				}
			}
	   ];
    
    return (
      <BarChart
        style={{ height: 200 }}
			data={barData}
      />
    );
  }
}
```

### 属性

| 属性              | 默认值          | 描述                                       |
| --------------- | ------------ | ---------------------------------------- |
| data            | **required** | 一个对象类型数组。数据结构为：{style? StyleProps,  positive?: StyleProps,  negative?: StyleProps, values: number[]} |
| labels          | undefined    | labels 可以让开发者提供给图表数据点的格式化标签。labels 函数的声明是： (args: { barIndex: number; valueIndex: number; data }) => string。barIndex是第几组（也就是 data 数组的索引），valueIndex 是在该组数据点的位置。 |
| style           | undefined    | [ViewStyleProps](https://facebook.github.io/react-native/docs/viewstyleproptypes.html) |
| spacing         | 0.05         | 每一个 bar 之间的间距。                           |
| contentInset    | {}           | 设置画布的四边的边距。比如我们给 chart 的 width 为 200，<br />则实际绘制的区域的有效宽度是 200 减去 contentInset 的 left 和 right |
| showMarker      | true         | 当 showMarker 启用时，用户在折线图上通过按下手指或者滑动手指可以实时显示一个 **marker**，类似在 web 上鼠标滑过时显示 **tooltip** 一样。这样的交互方式可以解决在手机上通过手指来浏览数据点的详细数据。 |
| renderGradient  | undefined    | 可以设置每一个 bar 的渐变渲染方式。                     |
| markerComponent |              | 通过这个函数，开发者可以自定义 marker 的显示样式。函数的声明为：({x, y, label, index, data}) => Component。返回的 component 是 react-native 组件。 |
| onPointPressIn  | Function     | 在用户按下一个数据块时触发这个事件。                       |
| onPointPressOut | Function     | 用户取消一个数据块的按下状态时触发这个事件。                   |
| onPointPress    | undefined    | 用户快速点击一个数据块时触发这个事件。                      |

#### onPointPressIn、onPointPressOut、onPointPress

这三个函数有相同的函数签名：

`(evt: ChartEventArgs) => ChartEventResult | undefined | null`

下面我们列出了 **ChartEventArgs**、**ChartEventResult** 的声明。

``` typescript
export interface ChartEventArgs {
    props: {
        style: StyleProps;
        index: number;
        data: any;
        label?: string;
    };
}

export interface ChartEventResult {
    target?: "data";
    mutation?: ChartMutation;
}

export interface ChartMutation {
    style?: StyleProps;
}

export interface StyleProps {
    fill?: string;
    stroke?: string;
    fillOpacity?: number;
    strokeOpacity?: number;
    [key: string]: any;
}
```

这里重点要阐述一下 **ChartMutation** 的作用。

通常，在 **onPointPressIn** 和 **onPointPressOut** 的时候，我们需要对用户的操作进行一些视觉上的反馈处理。而这里的 **ChartMutation** 就可以满足我们的要求。

我们在 **bar-chart** 的源码中可以看到默认的 **onPointPressIn** 和 **onPointPressOut** 的定义：

```typescript
...        
onPointPressIn: ({ props }) => {
  return {
    mutation: {
      style: Object.assign(props.style, {
        fillOpacity: 0.75 
      })
    }
  };
},
onPointPressOut: ({ props }) => {
    return {
      mutation: {
        style: Object.assign(props.style, {
          fillOpacity: 1
        })
      }
    };
}
..
```

在 **onPointPressIn** 中，我们返回结果中包含了一个 **mutation**，这个 **mutation** 将当前数据的 **fillOpacity** 样式改为了 0.75，而在 **onPointPressOut**，我们用同样的方式又将 **fillOpacity** 样式还原为了 1。这样，我们就可以让用户看到按下手指和抬起手指时数据的显示效果发生了一些细微的变化，实现了交互反馈。

