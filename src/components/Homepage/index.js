import PropTypes from 'prop-types';
import React from 'react';
import Helmet from 'react-helmet';
import './style.css';
import Header from '../Header';
import Heading from './Heading';
import Footer from '../Footer';
import Link from 'gatsby-link';
const Homepage = ({ users }) => (
	<div className="container">
		<Helmet title="Storybook - UI dev environment you'll love to use" />
		<Heading />
		<Link to="/intro/">文档</Link>
		<Footer />
	</div>
);

Homepage.propTypes = {
	featuredStorybooks: PropTypes.array, // eslint-disable-line
	users: PropTypes.array // eslint-disable-line
};

export default Homepage;
