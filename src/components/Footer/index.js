import React from 'react';
import Link from 'gatsby-link';
import slackIcon from './images/slack-icon.png';
import githubIcon from './images/github-icon.png';
import './style.css';

const Footer = () => (
	<div id="footer" className="row">
		<div className="col-md-12">
			<div className="row logos">
				<div className="col-xs-12">
					<center>本站点由畅捷通云-公共应用技术团队维护</center>
				</div>
			</div>
		</div>
	</div>
);

export default Footer;
