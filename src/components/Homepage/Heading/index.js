import React from 'react';
import './style.css';

const Heading = () => (
	<div id="heading" className="row">
		<div className="col-xs-12">
			<h3 className="sb-tagline">
				The UI Development Environment
				<br />
				You'll ♥️ to use
			</h3>
		</div>
	</div>
);

export default Heading;
