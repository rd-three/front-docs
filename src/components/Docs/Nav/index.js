import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import './style.css';
import './jquery.tocify.css';

const renderSectionItems = (section, selectedSectionId, selectedItemId) => {
  const elements = [];

  for (const item of section.items) {
    const isSelected =
      section.id === selectedSectionId && item.id === selectedItemId;
    const cssClass = isSelected ? 'selected' : '';
    const url = `/${section.id}/${item.id ? item.id + '/' : ''}`;

    elements.push(
      <li key={item.id}>
        <Link className={cssClass} to={url}>
          {item.title}
        </Link>
      </li>
    );

    if (isSelected) {
      elements.push(
        <div key={`toc_${item.id}`} id="toc" className="sub-toc" />
      );
    }
  }

  return elements;
};

class Nav extends React.Component {
  componentDidMount() {
    $('#nav').append(`
		<script>
			var toc = $("#toc")
			.tocify({
				context: "#docs-content",
				selectors: "h3",
				theme: 'none',
				scrollTo: 64 + 55,
				extendPage: false,
				hashGenerator: function(text) {
					return text.toLowerCase();
				}
			})
			.data("toc-tocify");
		</script>
	  `);
  }

  render() {
    const { sections, selectedSectionId, selectedItemId } = this.props;

    return (
      <div id="nav">
        {sections.map(section => (
          <div key={section.id}>
            <h3>{section.heading}</h3>
            <ul>
              {renderSectionItems(section, selectedSectionId, selectedItemId)}
            </ul>
          </div>
        ))}
      </div>
    );
  }
}

Nav.propTypes = {
  sections: PropTypes.array, // eslint-disable-line
  selectedSectionId: PropTypes.string.isRequired,
  selectedItemId: PropTypes.string.isRequired
};

export { Nav as default };
