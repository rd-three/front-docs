module.exports = {
	pathPrefix: `/front-docs`,
	siteMetadata: {
		siteTitle: '前端技术文档',
		baseColor: '#e64074',
		linkPrefix: '/',

		docSections: {
			intro: [ '/intro/' ],
			application: [ '/application/' ],
			graphql: [ '/graphql/intro/', '/graphql/advance/' ],
			chart: [ '/chart/intro/', '/chart/line-chart/', '/chart/pie-chart/', '/chart/bar-chart/' ],
			plugin: [ '/plugin/' ]
		}
	},
	plugins: [
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'pages',
				path: `${__dirname}/src/pages/`
			}
		},
		// {
		// 	resolve: `gatsby-source-filesystem`,
		// 	options: {
		// 		name: `data`,
		// 		path: `${__dirname}/src/data/`
		// 	}
		// },
		// {
		// 	resolve: `gatsby-source-filesystem`,
		// 	options: {
		// 		name: `images`,
		// 		path: `${__dirname}/src/assets/images/`
		// 	}
		// },
		{
			resolve: 'gatsby-transformer-remark',
			options: {
				plugins: [
					// {
					// 	resolve: 'gatsby-remark-images',
					// 	options: {
					// 		maxWidth: 690
					// 	}
					// },
					'gatsby-remark-autolink-headers',
					'gatsby-remark-copy-linked-files',
					'gatsby-remark-smartypants',
					`gatsby-remark-prismjs`
				]
			}
		},
		'gatsby-transformer-json',
		'gatsby-transformer-yaml',
		// 'gatsby-plugin-sharp',
		// 'gatsby-transformer-sharp',
		'gatsby-plugin-emotion',
		'gatsby-plugin-react-next'
	]
};
