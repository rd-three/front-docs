---
id: ''
title: 好生意移动端技术文档
---

### 背景

2018年，我们云公共应用平台研发的重头戏就是公司云战略中，全新的移动端的能力建设。我们的目标是在今年以全新的技术和思路构建一个可以支撑未来很长一段时间产品研发需要的移动端开发框架。

然而，一个优秀的开发框架绝不是突然形成的，也不能通过闭门造车的方式来设计和实现。它必须脱胎于我们的产品应用的需求，同时要高于我们实际产品应用的需求。

为了让我们在快速迭代的过程中，将我们的技术知识沉淀和传递下去。我们需要在过程中将我们的成果以文档的形式整理记录下来。

### 项目代码组织

好生意移动端项目的代码组织我们采用了 **packages** 的模式进行组织。**packages** 可以迫使我们在设计组织我们项目的代码功能时候一开始就需要按照独立 **package** 的模式进行思考和规划。从而避免我们以往在一个项目代码库中多种不同职责的代码相互交织相互引用，最终导致一个项目的代码在后期无法有效的进行拆分和重用以及重构。

感谢 **yarn** 支持的 **workspaces** 特性，让我们得以轻松的可以在单一代码库（monorepo）的代码库中使用  **packages** 来规划和拆分我们的代码职责。

当然，为了更好的管理这些 **packages**，我们采用了 **lerna** 这个优秀的开源库。**lerna** 是多package代码库的工作流的一个管理工具，可以让你在主项目下管理多个子项目，从而解决了多个包互相依赖，且发布时需要手动维护多个包的问题。

接下来，我们对好生意目前的 **packages** 做一个简单的介绍。

在项目的 **packages** 目录下的每一个子目录都是一个 **package**，它们有：

| package                 | package       name    | 用途                                       |
| ----------------------- | --------------------- | ---------------------------------------- |
| **application**         | hsy-mobile            | 好生意移动应用主程序包，这个包里面基本上应该只包含纯业务模块的内容。       |
| **assets**              | hsy-mobile-assets     | 资源包，项目中用到的公共的图标、图片、SVG 等都放在这里。           |
| **client**              | hsy-mobile-client     | 初始化 app 的 client 包，app 的 client 继承了 **nova-mobile-client**，在这里我们对 **application** 需要的访问服务器的 **client** 以及 **stores** 进行了初始化。 |
| **common**              | hsy-mobile-common     | 好生意应用需要的公共内容。                            |
| **components**          | hsy-mobile-components | 好生意应用公共UI组件。                             |
| **formula**             | hsy-mobile-formula    | 这个 package 封装了销货单的商品价格计算逻辑               |
| **stores**              | hsy-mobile-stores     | 整个好生意移动端应用的数据状态管理。                       |
| **theme**               | hsy-mobile-theme      | 应用的主题                                    |
| **types**               | hsy-mobile-types      | 这个 package 包含了通过我们代码和描述文件自动生成的 graphql typescript 类型定义和 graphql 的描述文件。 |
| **nova-metadata**       | nova-metadata         | 畅捷通 sass 应用的 BO 元数据服务。                   |
| **nova-metadata-form**  | nova-metadata-form    | 基于 nova-metadata 创建 simple-form 的 helpers |
| **nova-mobile-client**  | nova-mobile-client    | 移动端的 client 服务，提供了用户登录、退出、网络请求。          |
| **nova-native-plugins** | nova-native-plugins   | 这个 package 提供 native 的插件能力，比如从手机图册中选择图片、拍照、扫码、日期和时间选择、定位等等。 |
| **nova-shoutem-theme**  | nova-shoutem-theme    | 应用 theme 的基础能力                           |
| **nova-chart**          | nova-chart            | 图表，包含：Line、Bar、Area 等等图表类型。              |

以上 **packages** 我们再后续将逐一进行更详细去展开说明。

