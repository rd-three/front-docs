/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';

const HTML = props => {
  const title = DocumentTitle.rewind();

  let css;
  if (process.env.NODE_ENV === 'production') {
    // eslint-disable-next-line
    css = (
      <style
        dangerouslySetInnerHTML={{
          __html: require('!raw!../public/styles.css')
        }}
      />
    );
  }

  const searchScript = [
    <link
      key="docsearch-css"
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.css"
    />,
    <script
      key="docsearch-js"
      type="text/javascript"
      src="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.js"
    />
  ];

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/assets/css/jquery.ui.all.css" />
        <script src="/assets/js/jquery-1.8.3.min.js" />
        <script src="/assets/js/jquery-ui-1.9.1.custom.min.js" />
        <script src="/assets/js/jquery.tocify.js" />
        <title>{title}</title>
        {css}
        {props.headComponents}
      </head>
      <body>
        <div id="___gatsby" dangerouslySetInnerHTML={{ __html: props.body }} />
        {props.postBodyComponents}
      </body>
    </html>
  );
};

HTML.displayName = 'HTML';
HTML.propTypes = {
  headComponents: PropTypes.node.isRequired,
  postBodyComponents: PropTypes.node.isRequired,
  body: PropTypes.string
};
HTML.defaultProps = {
  body: ''
};

export default HTML;
