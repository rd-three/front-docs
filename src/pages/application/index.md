---
id: ''
title: 介绍
---

**Application** 包是我们好生意业务开发过程中最主要的部分，因为这里承载了好生意移动端所有的业务模块。

### modules

在 **application/src/modules** 目录中是我们应用开发的所有业务模块。什么是业务模块？简单的理解就是在交互中的一个页面功能的入口我们就可以称之为一个业务模块。

#### index.ts

我们先看一下 **modules/index.ts** 代码。

```typescript
// application/src/modules/index.ts
export function registerScreens() {
    require("./login").default();
    require("./goods-issue").default();
    require("./cust-vendor").default();
    require("./refer").default();
    require("./main").default();
    require("./my").default();
    require("./chart").default();
    require("./plugins").default();
}
```

可以看到，这里有一个 **registerScreens** 函数，我们在这个函数中统一管理我们应用的所有 **screens**。

让我们进入 **./login** 这个目录看一下 `require("./login").default()` 这一句做了什么。

```typescript
// application/src/modules/login/index.ts
...

export default function registerScreens() {
	registerScreen('LoginScreen', (require as any).resolve('./LoginScreen'), () => require('./LoginScreen').default);
}

function registerScreen(name: string, resolvedPath: string, screen: () => any) {
	Navigation.registerComponent(name, makeHot(screen, name));
	const hot = (module as any).hot;

	if (hot) {
		hot.accept(resolvedPath, () => {
			resetThemeCache();
			clearCacheFor(resolvedPath);
			redraw(screen, name);
		});
	}
}
```

这个文件中 export 了一个 **registerScreens** 函数作为默认导出对象。在 **registerScreens** 函数中，我们向 **react-native-navigation** 注册了一个名称叫 **LoginScreen** 的页面组件。同时 **registerScreen** 函数中将这个页面组件设置为可接受热更新，这样我们就可以修改页面组件中的内容而不需要将整个应用重新刷新。

有几点要注意的地方：

1. ` (require as any).resolve('./LoginScreen') `

   这里必须使用 ` require.resovle`  这个方式对页面组件进行解析，而不能直接传入 "./LoginScreen" 。

2. `() => require('./LoginScreen').default`

   这里要注意的是我们实际上注册页面组件的时候使用的是一个函数，函数返回了实际的页面组件。

3. `registerScreen` 函数为何不提成一个 utils 函数？其中的具体原因不是很清楚，但是可以明确的是如果我们不将 `registerScreen` 函数和调用这个函数的代码放到一个文件中。那么我们的热更新（hot-reload）就会失败。

> 在我们的规范中，所有的文件夹的名称都应该是全部小写。如果存在多个单词需要分割，可以使用 “-” 进行分割。





