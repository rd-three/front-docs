---
id: 'advance'
title: 高级
---

前面一章我们对好生意基于 BO 的 GraphQL 查询做了一个基本使用的介绍。那么这一章我们要讲的内容是什么呢？

### 动机

Sass 后端提供的 GraphQL 服务是完全基于 BO 元数据模型构建的 Schema。在我们设计好生意手机端和 Node Server 接口的时候，我们就希望尽量减少 REST API 或者 CUSTOM END POINT API 的规模，同时也向从 GraphQL 中获得更多的便利和好处。因此，我们希望在 Sass 后端提供的 GraphQL Schema 上能够进行扩展，扩展出手机端需要的 GraphQL Schema。

移动端扩张 BO 的 GraphQL Schema 的代码在如下图中红色框起来的位置：

![](media/15163268149091.jpg)

下面，让我们开始探寻如何扩展已有的 BO 类型。


### 扩展已有的 BO 类型

为什么需要对已有的 BO 类型的 Schema 进行扩展？我们知道， BO Schema 是基于我们的业务模型上按照约定规则自动生成的 Schema。也就是说，BO Schema 是业务模型和模型关系的反应。

而我们移动端的界面很多时候是取多个业务模型的数据的片段组合而成，并且有些数据是需要通过调用明确的 REST API来获取，无法通过 BO GraphQL 接口获取数据。

比如，我们在开单选择商品的时候，界面除了需要显示商品的基本信息，还需要显示某个客户购买这个商品的推荐价格。而推荐价格是无法从商品BO上直接获取，必须通过调用 REST API 获取。

通常，这种情况我们有两种实现思路：

- 一个思路是在移动端商品基本信息通过 GraphQL 接口获取，推荐价格通过 RESET API 获取。移动端需要发出两个不同的请求，并且在获取两个请求的结果后再进行组织。
- 一个是我们还是希望只需要一个 GarphQL 查询就可以获取商品基本信息和推荐价格。

显然，第二种思路相比第一种思路是更为先进和优雅的。那么，现在的问题就是，我们如何实现？

#### 第一步：扩展商品类型

在 `app-server/mobile/graphql/types` 下面新增一个 `product.ts` 文件。敲入下面的代码：

```typescript{2}
const typeDefs = `
extend type Product__type {}
`
export default typeDefs;
```

现在，我们通过 `extend type Product__type` 去扩展商品类型。为什么是 `Product__type` 而不是 `Product` ? 

首先，我们需要了解一个规则。BO 的 GraphQL ObjectType 的名称是按照 BOName + "\__type" 的统一规则自动生成的。比如，BO 的名称是 **CustVendor**，这这个 BO 在GraphQL 中的 ObjectType 名称则一定是 **CustVendor__type**。

#### 第二步：为商品类型添加新的字段

现在，我们给商品增加一个可以按照客户的推荐售价，继续编辑第一步的代码。

```typescript{3-7}
const typeDefs = `
extend type Product__type {
    # 推荐售价
    recommendedPrice(
        # 往来单位ID，可以忽略
        custVendorId: Long
    ): BigDecimal
}
`
```

如代码中所示，我们给商品类型添加了一个 `recommendedPrice`，这个字段接受一个 `custVendorId` 参数。返回值的类型为 `BigDecimal`。

接下来，我们将新增加的 `product.ts` 文件添加到 `app-server/mobile/graphql/types/index.ts` 文件中。

```typescript
import productTypeDefs from "./product";
...
export default [
    ...
    productTypeDefs,
]
```

> GraphQL 中支持的**基本数据类型**是：**String**、**Int**、**Boolean** 和 **Float**。

到此，我们已经扩展了已有的商品类型，并且添加了一个 **recommendedPrice** 字段。

这两步我们可以这样来理解：`Product__type` 我们可以理解为 `interface`，现在通过 `extend` 指令我们可以理解给这个 `interface` 增加了一个 `recommendedPrice` 字段。

好了，接口声明完了。然而只有接口声明没有实现是不能得到结果的。因此，接下来我们就开始实行我们添加的字段的取值方法。

#### 第三步：实现 recommendedPrice 字段的逻辑

我们知道，GraphQL查询或者操作（mutation）的 payload 都是由一组字段组成。在 GraphQL 服务器实现中，这些字段中的每一个实际上都对应于一个称为 [resolver](http://graphql.org/learn/execution/#root-fields-resolvers) 的函数。这个函数的作用就是提供对应字段的数据。

> - resolver 函数可以返回 Promise。
> - 这里有一个默认的隐含规则，即使你没有给一个字段提供明确的 resolver，实际上也有一个默认的 resolver，这个默认的 resolver 会尝试从 parent 对象中按照 field 的 name 获取对应的数据作为这个字段的值。

现在，让我们开始给 `recommendedPrice` 字段添加 resolver。

在 `app-server/mobile/graphql/resolvers` 下面新增一个 `product.resolver.ts` 文件。敲入下面的代码：

```typescript{6}
import * as _ from 'lodash';
import { GraphQLResolveInfo } from 'graphql';
import { IContext } from '../../../app-types';

export default {
    Product__type: {},
}
`
```

`Product__type` 表示我们接下来的 resolvers 是提供给 `Product__type` 这个 ObjectType 的。

继续编辑代码：

```typescript{7-10}
import * as _ from 'lodash';
import { GraphQLResolveInfo } from 'graphql';
import { IContext } from '../../../app-types';

export default {
    Product__type: {
        recommendedPrice: async({ id }, { custVendorId }, { ctx }: { ctx: IContext }, info: GraphQLResolveInfo) {
            ...
        }
    },
}
`
```

现在，我们在 `Product__type` 中添加了一个 `recommendedPrice` 的函数。这个 `recommendedPrice` 和我们在前面第二步给扩展类型添加的字段名称完全相同。

这个函数就是这个字段的 `resolver`，这里我们的函数是一个异步函数。

##### 函数签名
每一个 `resolver` 函数都接受 4 个参数：

```javascript
fieldName(obj, args, context, info) { result }
```

这 4 个参数的含义：
- `obj`： 一个包含从父字段中的解析器返回的结果的对象。比如在上面这个函数中，obj 参数就是你查询的一个具体的商品数据，这个数据中包含的字段就是你在 GraphQL 查询文档中请求的字段。
- `args`：查询中传递过来的参数。例如，如果该字段是用 `recommendedPrice（custVendorId：1）`调用的，则 args 对象将是：`{"custVendorId"："1"}`。
- `context`：这是所有解析器在特定查询中共享的对象，用于包含每个请求状态，包括身份验证信息，dataloader实例以及解析查询时应该考虑的任何其他内容。在我们这里主要就是 `ctx` 这个请求上下文对象了。
- `info`：这个参数只能在高级情况下使用，但是它包含查询执行状态的信息，包括字段名，从根到字段的路径等等。我们为了简化大家的使用成本，给大家提供了一些公共的处理这个 `info` 的 helper 方法。

##### 函数返回值

resolver 返回值的类型根据不同的情况会是下面这几种：

- `null` 或者 `undefined`：这表示没有返回值。如果您的模式表示该字段可以为空，那么结果将在该位置具有空值。如果该字段为非空，则结果将“冒泡”到最近的可空字段，并且该结果将被设置为空。
- `array`：这只有在 Schema 中该字段的定义是列表时才有效。查询的子选择器将为此数组中的每个项目运行一次。
- `promise`：解析器通常会执行异步操作，如从数据库或后端 API 中获取数据，所以它们可以返回promise。这可以与数组结合使用，因此解析器可以返回一个解析为数组的 promise，或者 promise 数组。
- `scalar` 或者 `object`： 一个解析器也可以返回任何其他类型的值，它没有任何特殊的含义，只是向下传递给嵌套的子解析器。

##### recommendedPrice 更具体的实现

刚才巴拉巴拉巴拉的讲了一通 resolver 函数的签名和返回值，估计你可能已经晕掉了。下面我们还是直接点，来点更为实际的。

假设我们后端可爱的同学给我们提供了一个 `biz/getRecommendedPrice` API，这个 API 可以接受`商品ID`和`客户ID`两个参数，并且给我们返回给这个客户该商品的推荐售价。

那么，我们可以在上面的函数中这样调用这个 API。

```javascript
recommendedPrice: async({ id }, { custVendorId }, { ctx }: { ctx: IContext }, info: GraphQLResolveInfo) {
    const price = await ctx.service.core.api.get('/biz/getRecommendedPrice', {
        params: {
            productId: id,
            custVendorId: custVendorId
        }
    });

    return price;
}
```

到此，我们已经成功完成了任务！但是，请不要着急的关闭这个文档，**请继续看下去**。

上面的 `resolver` 实现看着好像是没有问题的。但是，这里将会出现一个严重的性能问题。请你思考一会儿问题在哪里，如何解决这个问题？然后可以到这篇文档下面的 `DataLoader` 章节寻找答案。


### 增加全新的类型

上面我们学习到了如何扩展已经存在的 GraphQL 类型。实际上我们有时候需要全新的新类型来响应前端页面的数据查询或者操作需求。现在，就让我们一起来学习如何增加一个全新的 GraphQL 类型。

现在，假设我们的前端有一个页面需要今天的销售统计数据指标，这些指标包括：

- 成交订单数量 - `orderAmount`
- 退货订单数量 - `returnOrderAmount`
- 成交新客户数量 - `newCustomerAmount`

#### 第一步：增加销售统计数据指标类型

在 `app-server/mobile/graphql/types` 下面新增一个 `sales-kpi.ts` 文件。敲入下面的代码：

```typescript
const typeDefs = `
# 销售统计数据指标
type SalesKPI__type {
    # 成交订单数量
    orderAmount: BigDecimal
    # 退货订单数量
    returnOrderAmount: BigDecimal    
    # 成交新客户数量
    newCustomerAmount: BigDecimal 
}
`
export default typeDefs;
```

和前面扩展已有类型不同的是我们现在使用的是 `type SalesKPI__type` 语句。而不是 `extend type ...` 语句。

这个语句声明了一个新的 `SalesKPI__type` GraphQL 类型。这个类型包括了三个字段。

> `BigDecimal` 是我们自定义的一个 `scala` 类型，意思是这个值可能是一个超过16位有效位的数值。当然，我们实际上不需要关心这个，我们只需要把它看成是 javascript 中的 number 就好了。


#### 第二步：增加指标的查询类型

第一步中，我们增加了销售统计数据指标类型。不过我们还需要提供一个查询类型给使用者来获取数据。

接下来，还是在 `sales-kpi.ts` 中继续敲入代码：

```javascript{7-13}
const typeDefs = `
# 销售统计数据指标
type SalesKPI__type {
  ...
}

extend type Query {
    # 查询销售统计数据指标
    salesKpi (
        # 查询哪一天的数据
        # 如果不提供，则默认为今天
        date: Long
    ): SalesKPI_type
}
`
export default typeDefs;
```

我们通过 `extend type Query` 语句给 GraphQL 查询对象增加了一个 `salesKpi` 的查询字段。

这个查询字段提供了一个 `date` 参数，使用者可以通过这个参数要求返回指定某一天的统计指标。查询字段返回值的类型为 `SalesKPI__type`。

接下来，我们将新增加的 `sales-kpi.ts` 文件添加到 `app-server/mobile/graphql/types/index.ts` 文件中。

```typescript
import salesKpiTypeDefs from "./sales-kpi";
...
export default [
    ...
    salesKpiTypeDefs,
]
```

> 实际上，所有的查询字段都是 `Query` 这个特殊类型的字段。

#### 第三步：实现 SalesKPI 的 Resolver

现在，让我们开始给 `SalesKPI` 字段添加 resolver。

在 `app-server/mobile/graphql/resolvers` 下面新增一个 `salesKpi.resolver.ts` 文件。敲入下面的代码：

```typescript{6}
import { GraphQLResolveInfo } from 'graphql';
import { IContext } from '../../../app-types';

export default {
    Query: {
        salesKpi: async(obj, { date }, { ctx }, info) {
            // 调用后端提供的 api 获取数据
            // 假设返回的数据标准是：
            // { 
            //    orderAmount, 
            //    returnOrderAmount,
            //    newCustomerAmount,
            // }
            const result = await ...
            return result;
        }
    },
}
`
```


### DataLoader 介绍

#### 什么是 DataLoader？

[Dataloader](https://link.jianshu.com/?t=https://github.com/facebook/dataloader) 是由facebook推出，能大幅降低数据库的访问频次，经常在Graphql场景中使用。

#### DataLoader 机制

主要通过2个机制来降低数据库的访问频次：`批处理` 和 `缓存`。

- 批处理

    在一个Nodejs任务执行单元中[1]，如果多次调用同一类数据，为避免数据库访问，Dataloader可以使用传入的批量处理函数一次访问后台服务。
    
    
    ![](https://upload-images.jianshu.io/upload_images/2642929-61758d9d7ee4ce2c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/700)

- 缓存
    
    load一次，DataLoader就会把数据缓存在内存，下一次再load时，就不会再去访问后台。
    
    DataLoader缓存的是promise，而不是具体数据。
    
    我们的 DataLoader 没有用缓存这个机制是因为我们希望每一次请求的时候都获取的是后端的新的数据。

#### 实现原理

DataLoader 的实现原理是：把每一次 load 都推迟到 Next Tick 后集中批处理运行。

具体的说就是：DataLoader把个 Nodejs 任务执行单元中的多个 load 操作集中放到 promise job queue 中，并使用 DataLoader 初始化时传入的批量操作完成。

#### 解决的问题

Dataloader 并不是一个超级工具，它的使用场景是有限的。基本上只能优化 loadById 这种场景。

Dataloader 的最佳使用场景是解决 Graphql 中的 N+1 查询问题，比如下面的查询：

```json
# 定义
type User {
  name: String,
  friends: [User]
}

#查询
{
  users {
    name
    friends {
      name
      friends {
        name
      }
    } 
  }
}
```

上面的 Graphql 查询如果没有 Dataloader，就会出现严重的 N+1 查询性能问题，而通过使用 Dataloader，数据库的访问频次指数级别下降。


### BODataLoader 

上面简单的介绍了 DataLoader，现在让我们开始了解在 `app-server/mobile/dataloader/` 中提供的 DataLoader。

在 `dataloader` 目录下的 `index.ts` 文件我们可以实现了一个 `DataLoaderService` 类。这个类会在每次请求的时候初始化一个实例，这个实例可以通过请求上下文 `ctx.dataloader` 来访问。

`ctx.dataloader` 有一个只读的 `boDataLoader` 属性，这个属性中提供了一些针对 BO 查询的通用并且有用的 DataLoader 实现。


#### loaderManyObjects 方法

这个函数可以让我们可以根据传入的 BO 查询参数一次性批量获取数据。

让我们以一个简单的例子来说明这个函数的使用场景。首先我们先对 `CustVendor__type` 进行扩展，增加一个 `latestGoodsIssue` 字段，该字段的返回值类型为一个 `GoodsIssue__type` BO 类型。

```json
// 扩展 CustVendor__type 类型
extend type CustVendor__type {
    # 最近交易的销货单
    latestGoodsIssue: GoodsIssue__type
}
```
 
在 `latestGoodsIssue` 的 resolver 中的代码片段：

```typescript
CustVendor__type: {
	latestGoodsIssue: async ({ id }, args, { ctx }, info) => {
		const options: BOBatchLoaderContext = {
			info,
			queryType: 'GoodsIssue',
			keyField: 'soldToCustId',
			excludeKeyFieldAsCriterial: true,
			args: {
				criteriaStr: `id in (select max(t.id) from GoodsIssue as t where t.soldToCustId in (:ids) group by t.sold_to_cust_id order by t.biz_date desc)`
			}
		};

		const loader = ctx.dataloader.boDataLoader.loaderManyObjects(options);
		return loader.load(id);
	};
}
```

上面的代码返回了往来单位最近的一笔销货单数据。要理解和使用这个函数，首先我们要了解 `LoadManyObjectsContext` 这个传递给 `loaderManyObjects` 函数的参数类型：

- `queryType` - `string`
    这个 loader 的 BO 查询类型名称。注意，BO 的 `queryType` 名称就是 BO 的名称，而不是 BoName + "__type"。
- `keyField` - `string`
    在 loader 返回查询结果后通过这个 `keyField`在查询结果中的找到和 **loader.load(id)** 中传入的 id 相匹配的数据。比如上面的代码中，`keyField` 的值为 `soldToCustId`，如果我们给 **loader.load** 方法提供的 id 为 **1**, 则在查询结果中我们会把 GoodsIssue.soldToCustId 值等于 1 的数据返回给这个 **latestGoodsIssue** 字段。
- `args` - `GqlAggrQueryArguments`
    这个参数不解释了，请参考 [查询参数](/graphql/intro/#%E6%9F%A5%E8%AF%A2%E5%8F%82%E6%95%B0)
- `info` - `GraphQLResolveInfo` 
    GraphQL 文档中这个字段结构详细信息。这里不做深入说明，有兴趣可以自己看看这个的结构声明。
- `excludeKeyFieldAsCriterial` - `boolean`
    true 表示在查询 `queryType` 时，不自动在 `criteriaStr` 中加入 `keyField` 作为查询条件。默认为 false，比如这个例子中 excludeKeyFieldAsCriterial 值就为 false。
    
#### composedLoader

这个函数最主要的作用就是把多个对 BO 的查询合并到一个对后端的 GraphQL 文档中。这样可以在一次 HTTP 请求中返回多个 GraphQL 查询要求，减少网络往返开销，可以极大的提高响应性能。

实际上，如果我们深入了解上面提到的 `loaderManyObjects` 函数的实现代码，就会发现其中也用到了这个函数。


![](media/15164510726782.png)

这个函数的使用比较简单，简单说就是将多个分散的 GraphQL 查询请求合并到一个查询请求中。

#### 需要了解的 Common 中的一些函数

在 `mobile/dataloader/common.ts` 中提供了一些公共的函数。其中有 `resolveInfoToString` 是我们最需要了解也可能经常用到的。

##### resolveInfoToString

这个函数接受一个 `GraphQLResolveInfo` 类型的参数，然后解析这个参数，返回一个 GraphQL 查询片段。在这个片段中，不包括我们扩展的字段，因为这些扩展字段在后端的 BO Schema 中不存在。片段中的扩展字段会在查询执行时自动执行这些扩展字段的 resolvers。

多数情况下，这个函数是在我们调用 `composedLoder` 的时候使用的。`composedLoader` 函数接受参数是 `BatchLoaderContext` 类型，这个类型需要一个 `fields`, 也就是查询的 GraphQL 字段片段。

在销售开单选择客户时，我们需要显示 ”常用客户“。在我们的查询文档中是这样的：

![](media/15165840493165.jpg)

`recentUseCustVendors` 是我们扩展的一个字段，其返回类型是 `CustVendor__type` 数组。因此这个字段的 `resolver` 最终仍然需要调用后端的 GraphQL 查询。而调用后端的 GraphQL 查询就需要我们将 `recentUseCustVendors` 中的 fields 解析出来。这就需要用到 `resolveInfoToString` 函数。

具体的使用方式如下图中的代码。

![](media/15165838662965.jpg)


### 自定义 DataLoader

有时候 BODataLoader 不能解决我们的问题，这多半是出现在我们扩展的字段只能通过 RESET API 而不能通过 BO GraphQL 请求得到结果。这种情况下我们就需要增加新的 DataLoader。

添加新的 DataLoader 很简单。首先我们需要知道如何创建一个 DataLoader。在 `mobile/dataloader/common.ts` 中 export 了一个函数：

``` typescript
function createDataLoader<TKey, TOut>(batchLoadFn: BatchLoadFn<TKey, TOut>): IDataLoader<TKey, TOut>
```

createDataLoader 接受一个函数，这个函数就是批量执行函数。这个 `batchLoadFn` 的签名如下：

``` typescript
type BatchLoadFn<TKey, TOut> = (keys: Array<TKey>) => Promise<Array<TOut>> | void
```

batchLoadFn 只接受一个参数 `keys`，这个 keys 是一个数组。

现在，让我们开始具体的示范，现在我们需要给商品提供按照包装单位的 **可用量格式化文本** 和 **现存量格式化文本** 这两个字段。这两个字段只能通过 REST API 获取。 现在对 `Product__type` 扩展：

```json
extend type Product__type {
    # 按包装单位的可用量格式化文本
    availableAmountText: String
    
    #按包装单位的现存量格式化文本
    stockAmountText: String
}
```

#### 第一步：创建一个 DataLoader

在 `mobile/dataloader/index.ts` 中，创建一个 DataLoader：

```typescript
class DataLoaderService {
	readonly boDataLoader: BODataLoader;
	//（1）声明一个只读的 loader 变量
	readonly productAmountTextLoader: IDataLoader<number, { 
        availableAmountText: string; stockAmountText: string 
    }>;

	constructor(readonly ctx: IContext) {
		this.boDataLoader = new BODataLoader(ctx);
		//（2）创建 loader
		this.productAmountTextLoader = this.createProductAmountLoader();
	}

	private createProductAmountLoader(): IDataLoader<number, { 
        availableAmountText: string; stockAmountText: string 
    }> {
      // (3) 通过 createDataLoader 创建一个 DataLoader 
		return createDataLoader<number, { 
            availableAmountText: string; 
            stockAmountText: string 
        }>(async (keys) => {
            // (4)以 keys 作为批量条件调用 rest api
            const result = await this.ctx.service.api.productAmountText.get(...);
            // (5) 以 keys 的原有顺序返回结果。
            return keys.map(key => {
                // 在 result 中找到 和 key 匹配的结果
                // 假如我们匹配结果是 matchItem
                return {
                    availableAmountText: matchItem.s1,
                    stockAmountText: matchItem.s2,
                }
            })
        });
	}
}
```

我们将代码中的几步关键步骤整理出来：

- (1) 声明一个只读的 productAmountTextLoader 变量
- (2) 创建 productAmountTextLoader
- (3) 通过 createDataLoader 创建一个 DataLoader
- (4) 以 keys 作为批量条件调用 rest api
- (5) 以 keys 的原有顺序返回结果

#### 第二步：调用 DataLoader

DataLoader 创建好后，调用它就很简单了，在 resolver 中：

```typescript

Product__type {
    availableAmountText: async({id}, args, {ctx}) {
        const result = await ctx.dataloader.productAmountTextLoader.load(id);
        return result.availableAmountText;
    },
    
    stockAmountText: async({id}, args, {ctx}) {
        const result = await ctx.dataloader.productAmountTextLoader.load(id);
        return result.stockAmountText;
    }
}
```




