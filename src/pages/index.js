import React from 'react';
import { values } from 'lodash';

class RedirePage extends React.Component {
	componentDidMount() {
		window.location = '/intro/';
	}

	render() {
		return null;
	}
}

export default () => <RedirePage />;
